<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "sendmail", "mailgun", "mandrill", "ses",
    |            "sparkpost", "log", "array"
    |
    */

    'menu' => array(
        array(
            "name"  => "Dashboard",
            "icon"  => "fa fa-dashboard",
            "route" => 'dashboard'
        ),array(
            "name"  => "Documents",
            "icon"  => "fa fa-folder",
            "has-sub" => array(
                array(
                    "name" => "Add New Document",
                    "route" => 'documents.add.form'
                ),array(
                    "name" => "All Documents",
                    "route" => 'documents.all'
                ),array(
                    "name" => "Archived Documents",
                    "route" => '#'
                )
            )
        ),array(
            "name"  => "Departments",
            "icon"  => "fa fa-university",
            "has-sub" => array(
                array(
                    "name" => "Add New Department",
                    "route" => 'departments.add.form'
                ),array(
                    "name" => "All Departments",
                    "route" => 'departments.all'
                ),

                // array(
                //     "name" => "Delete Documents",
                //     "route" => '#'
                // ),array(
                //     "name" => "Access Rights",
                //     "route" => '#'
                // )
            )
        ),array(
            "name"  => "Users",
            "icon"  => "fa fa-users",
            "has-sub" => array(
                array(
                    "name" => "Add New User",
                    "route" => 'users.add.form'
                ),array(
                    "name" => "All Users",
                    "route" => 'users.all'
                ),

                // array(
                //     "name" => "Suspended Users",
                //     "route" => '#'
                // ),array(
                //     "name" => "Banned Users",
                //     "route" => '#'
                // )
            )
        ),array(
            "name"  => "Reports",
            "icon"  => "fa fa-file-text-o",
            "has-sub" => array(
                array(
                    "name" => "User Activities",
                    "route" => '#'
                )
            )
        )
    ),


    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "sendmail", "mailgun", "mandrill", "ses",
    |            "sparkpost", "log", "array"
    |
    */

    'option' => [
        "showSearch"    => false,
        "showUser"      => true
    ]

];
