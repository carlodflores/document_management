<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');

Route::prefix('u')->group(function () {
    Route::get('/dashboard', 'DashboardController@index')                                       ->name('dashboard');

    Route::prefix('documents')->group(function () {
        Route::get('/all', 'DocumentController@index')                                          ->name('documents.all');
        Route::get('/{id}/view', 'DocumentController@view')                                          ->name('documents.view');

        Route::get('/add', 'DocumentController@showAddForm')                                    ->name('documents.add.form');
        Route::post('/add', 'DocumentController@addProcess')                                     ->name('documents.add.process');
        Route::post('/add/image', 'DocumentController@uploadImage')                                     ->name('documents.image.process');

        Route::get('/{id}/edit', 'DocumentController@editForm')                                          ->name('documents.edit.form');
        Route::post('/{id}/edit', 'DocumentController@editProcess')                                     ->name('documents.edit.process');
    });

    Route::prefix('departments')->group(function () {
        Route::get('/all', 'DepartmentController@index')                                          ->name('departments.all');

        Route::get('/add', 'DepartmentController@showAddForm')                                    ->name('departments.add.form');
        Route::post('/add', 'DepartmentController@addProcess')                                    ->name('departments.add.process');

        Route::get('/{id}/edit', 'DepartmentController@showEditForm')                                    ->name('departments.edit.form');
        Route::post('/{id}/edit', 'DepartmentController@editProcess')                                    ->name('departments.edit.process');

        Route::post('/{id}/delete', 'DepartmentController@deleteProcess')                                    ->name('departments.delete.process');
    });


    Route::prefix('users')->group(function () {
        Route::get('/all', 'UserController@index')                                          ->name('users.all');

        Route::get('/add', 'UserController@showAddForm')                                    ->name('users.add.form');
        Route::post('/add', 'UserController@addProcess')                                    ->name('users.add.process');

        Route::get('/{id}/edit', 'UserController@showEditForm')                                    ->name('users.edit.form');
        Route::post('/{id}/edit', 'UserController@editProcess')                                    ->name('users.edit.process');

        Route::post('/{id}/delete', 'UserController@deleteProcess')                                    ->name('users.delete.process');
    });
});
