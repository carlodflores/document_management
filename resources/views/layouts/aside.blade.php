<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    @if (config('aside.option.showUser'))
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->firstname . ' ' . Auth::user()->lastname }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
    @endif
    <!-- search form -->
    @if (config('aside.option.showSearch'))
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>
    @endif
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>

    @foreach (config('aside.menu') as $key => $menu)
          @php

            $active = false;
            if (isset($menu['has-sub'])) {
                foreach ($menu['has-sub'] as $key => $sub) {
                    if ($sub['route'] == Route::currentRouteName()) {
                        $active = true;
                        break;
                    }
                }
            } else if($menu['route'] == Route::currentRouteName()) {
                $active = true;
            }

          @endphp
          <li class="{{ $active ? 'active  menu-open' : '' }} {{ isset($menu['has-sub']) ? 'treeview' : '' }}">
            <a href="{{ isset($menu['route']) ? route($menu['route']) : '#'}}">
              <i class="{{ $menu['icon'] }}"></i> <span>{{ $menu['name'] }}</span>
             @if (isset($menu['has-sub']))
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
             @endif
            </a>
            @if (isset($menu['has-sub']))
                <ul class="treeview-menu">
                  @foreach ($menu['has-sub'] as $key => $sub)
                    <li class="{{ $sub['route'] == Route::currentRouteName() ? 'active' : '' }}">
                        <a href="{{ $sub['route'] == '#' || $sub['route'] == '' ? '#' : route($sub['route']) }}">
                            <i class="fa {{ $sub['route'] == Route::currentRouteName() ? 'fa-circle' : 'fa-circle-o' }}"></i>{{ $sub['name'] }}
                        </a>
                    </li>
                  @endforeach
                </ul>
            @endif
        </li>
    @endforeach

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
