@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="" action="{{ route('departments.add.process') }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <h2>Department Details</h2>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="">Title</label>
                        <input name="department_name" type="text" class="form-control">
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-success">Add Department</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
