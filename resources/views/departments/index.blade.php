@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Count</th>
                        <th>Option</th>
                    </thead>

                    <tbody>
                        @foreach ($departments as $key => $department)
                            <tr>
                                <td>{{ $department->id }}</td>
                                <td>{{ $department->title }}</td>
                                <td>{{ $department->members()->count() }}</td>
                                <td>
                                    <form action="{{ route('departments.delete.process', $department->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <a href="{{ route('departments.edit.form', $department->id) }}"><button type="button" class="btn btn-info btn-xs">Edit</button></a>
                                        <button class="btn btn-danger btn-xs">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
