@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Fullname</th>
                        <th>Department</th>
                        <th>Option</th>
                    </thead>

                    <tbody>
                        @foreach ($users as $key => $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->fullname() }}</td>
                                <td>
                                    @foreach ($user->departments as $key => $dept)
                                        <span class="label label-warning">{{ $dept->department->title }}</span>
                                    @endforeach
                                </td>
                                <td>

                                    <form action="{{ route('users.delete.process', $user->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <a href="{{ route('users.edit.process', $user->id) }}"><button type="button" class="btn btn-info btn-xs">Edit</button></a>
                                        <button class="btn btn-danger btn-xs">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
