@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="" action="{{ route('users.add.process') }}" method="post">
                    {{ csrf_field() }}

                    <div class="col-md-12">
                        <h3>User Information</h3>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">First Name</label>
                        <input name="firstname" type="text" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">Middle Name</label>
                        <input name="middlename" type="text" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">Last Name</label>
                        <input name="lastname" type="text" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">Email</label>
                        <input name="email" type="text" class="form-control">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-4">
                        <label for="">Password</label>
                        <input name="password" type="password" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="">Re-Password</label>
                        <input name="repassword" type="password" class="form-control">
                    </div>

                    <div class="col-md-12">
                        <h3>Department</h3>
                    </div>


                    <div class="form-group col-md-4">
                        <label for="">Add to Department</label>
                        <select name="department[]" multiple id="" class="form-control">
                            @foreach ($departments as $key => $department)
                                <option value="{{ $department->id }}">{{ $department->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-success">Add User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
