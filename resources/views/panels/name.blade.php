<section class="content-header">
  @if (isset($name))
      <h1>
        {{ $name }}
        @if(isset($sub)) <small>{{ $sub }}</small> @endif
      </h1>
  @endif
  @if ($breadcrumbs)
      <ol class="breadcrumb">
        @foreach ($breadcrumbs as $key => $li)
            <li class="{{ $key == count($breadcrumbs)-1 ? 'active' : '' }}">
                @if(isset($li['url'])) <a href="{{ isset($li['url']) ? $li['url'] : '#' }}"> @endif
                    @if (isset($li['icon']))
                        <i class="{{ $li['icon'] }}"></i>
                    @endif
                    {{ $li['name'] }}
                @if(isset($li['url'])) </a> @endif
            </li>
        @endforeach
      </ol>
  @endif
</section>
