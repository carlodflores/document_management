@extends('layouts.app')

@section('content')
<div class="row">

    @foreach ($documents as $key => $document)

        @php
            $access = array();
            foreach ($document->accesses as $key => $dept) {
                $access[] = $dept->access_id;
            }
        @endphp

        @if (!array_intersect($access, $check_access()) && $document->user->id != Auth::user()->id)
            @continue
        @endif
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="box box-default box-solid" style="height: 150px;">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $document->title }}</h3>
              <div class="box-tools pull-right">

                  <div class="btn-group pull-right" style="margin-left: 15px;">
                   <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                     <i class="fa fa-bars"></i></button>
                       <ul class="dropdown-menu pull-right" role="menu">
                         <li><a href="{{ route('documents.view', $document->id) }}" target="_blank"><i class="fa fa-eye fa-fw"> </i> View</a></li>
                         <li><a href="{{ route('documents.edit.form', $document->id) }}"><i class="fa fa-pencil fa-fw"> </i> Edit</a></li>
                         <li><a href="#"><i class="fa fa-folder fa-fw"> </i> Archive</a></li>
                       </ul>
                 </div>


                 <h3 class="no-margin has-sub pull-right"> <small> Pages</small> {{ count($document->contents) }}</h3>


              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="full-width">
                    <tr>
                        <td style="width: 50%;">
                            <small>Departments</small><Br/>
                            @foreach ($document->accesses as $key => $dept)
                                <span class="label label-warning">{{ $department($dept->access_id)->title }}</span>
                            @endforeach
                        </td>
                        <td>
                            <small>Uploaded By</small>
                            <h4 class="no-margin">{{ $document->user->fullname() }}</h4>
                        </td>
                        <td>
                            <small>Date Created</small>
                            <h4 class="no-margin">{{ date('m.d.Y', strtotime($document->created_at)) }}</h4>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    @endforeach


</div>
@endsection
