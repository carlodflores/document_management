<title>{{ $document->title . '--' . date('m-d-y', strtotime($document->created_at)) }}</title>

<style>
.page-break {
    page-break-after: always;
}

img {
    max-width: 93%;
}
</style>

<center>
    <h1 style="margin-bottom: 0px;">{{ $document->title }}</h1>
    <small>{{ $document->description }}</small>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/>
    <small>Compiled By : </small>
    <h3 style="margin-top: 0px;">{{ $document->user->firstname . ' ' . $document->user->lastname }}</h3>
</center>
<div class="page-break"></div>
@foreach ($document->contents as $key => $value)
    <img src="{{ asset($value->image) }}" alt="">
    <div class="page-break"></div>
@endforeach
