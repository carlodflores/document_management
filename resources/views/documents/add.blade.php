@extends('layouts.app')

@section('content')


<style media="screen">
    .overlay {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.8);
        z-index: 9999999;
        color: #fff;
        display: none;
    }

    .overlay div {
        width: 100%;
        position: absolute;
        top: 50%;
        margin-top: -5%;
    }
</style>

<div class="overlay">
    <center>
        <div class="">
            <h1>Currently Uploading...</h1>
            <p>Please do not leave or close this window. Thank you.</p>
        </div>
    </center>
</div>

<div class="row">
    <div class="col-md-2">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Select Files</h3>
        </div>
        <div class="box-body align-center">
            <form id="files">
                <label for="file" class="btn btn-primary btn-lg">
                    Select Scanned Documents
                    <input type="file" name="files[]" multiple class="hidden" id="file">
                </label>

                <h3><span id="status">Progress</span> : <span id="current">0</span>/<span id="total">0</span></h3>
                <small id="progress"></small>
            </form>
        </div>
      </div>

      <form id="details">
          <div class="box box-default">
              <div class="box-header with-border">
                  <div class="box-title">
                      Document Information
                  </div>
              </div>

              <div class="box-body">
                  <div class="form-group">
                      <label for="">Document Title</label>
                      <input type="text" name="title" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Document Description</label>
                      <textarea name="description" id="" cols="30" rows="5" class="form-control"></textarea>
                  </div>
              </div>
          </div>

          <div class="box box-default">
              <div class="box-header with-border">
                  <div class="box-title">
                      Document Settings
                  </div>
              </div>

              <div class="box-body">
                  <div class="form-group">
                      <label for="">For Departments</label>
                      <select name="departments[]" multiple id="" class="form-control">
                          @foreach ($departments as $key => $department)
                              <option value="{{ $department->id }}">{{ $department->title }}</option>
                          @endforeach
                      </select>
                  </div>

                  {{-- <div class="form-group">
                      <label for="">Available For Users</label>
                      <select name="user_avail[]" multiple id="" class="form-control">
                          @foreach ($users as $key => $user)
                              <option value="{{ $user->id }}">{{ $user->firstname . ' ' . $user->lastname }}</option>
                          @endforeach
                      </select>
                  </div> --}}
              </div>
          </div>
      </form>

      <center>
          <button class="btn btn-success" id="addDocuments" disabled>Add Document</button>
      </center>
    </div>

    <form id="document_content">
        <input type="hidden" name="active_pages" value="">
        <div class="col-md-10" id="page-holder">
            {{-- <div class="col-md-3">
              <div class="box box-warning box-solid">
                  <div class="box-header with-border">
                    <h4 class="box-title">OCR Confidence : @{{ conf }}%</h4>
                    <div class="box-tools pull-right">
                       <h3 class="no-margin has-sub"> <small> Page</small>  <input type="text" class="page" name="pages[]" value="@{{ page }}"></h3>
                    </div>
                  </div>
                  <div class="box-body">

                      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#img_@{{ page }}" data-toggle="tab" aria-expanded="true">Image Preview</a></li>
              <li class=""><a href="#content_@{{ page }}" data-toggle="tab" aria-expanded="false">Image Content</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="img_@{{ page }}">
                  <img src="@{{ img }}" alt="">
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="content_@{{ page }}">
                <textarea name="content[]" class="form-control" style="height: 200px;">@{{ result.text }}</textarea>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>



                  </div>
              </div>
            </div> --}}
        </div>
    </form>
</div>

<script id="entry-template" type="text/x-handlebars-template">
    <div class="col-md-3" data-id="@{{ item }}">
      <div class="box box-warning box-solid">
          <div class="box-header with-border">
            <h4 class="box-title"> <small>Accuracy @{{ conf }}%</small></h4>
            <div class="box-tools pull-right">
               <h3 class="no-margin has-sub"> <small> Page</small>  <input type="text" class="page" name="pages[]" value="@{{ page }}"> <button class="btn btn-danger btn-xs" data-target="@{{ item }}">x</button></h3>
            </div>
          </div>
          <div class="box-body">

          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class=""><a href="#img_@{{ page }}" data-toggle="tab" aria-expanded="true">Image Preview</a></li>
              <li class="active"><a href="#content_@{{ page }}" data-toggle="tab" aria-expanded="false">Image Content</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane " id="img_@{{ page }}">
                  <img src="@{{ img }}" alt="">
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="content_@{{ page }}">
                <textarea name="txt_img_@{{ page }}" class="form-control" style="height: 200px;">@{{ text }}</textarea>
                <input type="hidden" name="url_img_@{{ page }}" value="@{{ url }}">
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>



          </div>
      </div>
    </div>
</script>

@endsection

@section('script')
    <script src="{{ asset('bower_components/handlebars/handlebars.min.js') }}" charset="utf-8"></script>
    <script src='https://cdn.rawgit.com/naptha/tesseract.js/1.0.10/dist/tesseract.js'></script>

    <script type="text/javascript">



        var scanned_documents = new FormData();
        var counter = 0;
        var page = 0;
        var page_active = [];
        var processed = [];
        var reserved = {};
        $('input[name="files[]"]').change(function() {

            var ins = this.files.length;
            var fd = new FormData();

            for (var x = 0; x < ins; x++) {
                fd.append('img_' + (counter+1), this.files[x]);
                scanned_documents.append("img_" + (counter+1), this.files[x]);
                counter++;
            }

            fd.append('_token', '{{ csrf_token() }}');

            $.ajax({
                url : '{{ route('documents.image.process') }}',
                type : 'POST',
                data : fd,
                contentType: false,
                processData: false,
                success : function(data) {
                    var j = $.parseJSON(data);
                    $.each(j, function(k, v) {
                        var key = v.key;
                        reserved[key] = v.url;
                    });

                    console.log(data);
                },
                complete: function(data) {
                    imagesPreview('#page-holder')
                }
            })



        });

        $(document).on('click', '[data-target]', function() {
            var target = $(this).data('target');
            $('[data-id="'+ target +'"]').remove();
            page_active.splice(page_active.indexOf(target), 1);
            scanned_documents.delete('img_' + target);
            $('input[name="active_pages"]').val(page_active.join(','));
        });

        $('#addDocuments').click(function() {

            var datas = $('#details, #document_content').serializeArray();

            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            $('.overlay').fadeIn();
                        }
                   }, false);

                   return xhr;
                },
                url : '{{ route('documents.add.process') }}',
                type : 'POST',
                data : {data : datas},
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success : function(data) {
                    var j = $.parseJSON(data);

                    if (j.status == 1) {
                        window.location = j.url;
                    } else {
                        $('.overlay').fadeOut();
                    }

                }
            }).fail(function () {
    			alert('Cannot Connect to Server.');
                $('.overlay').fadeOut();
    		});
        });


        var imagesPreview = function(placeToInsertImagePreview) {
            var ctr = 0;
            if (scanned_documents.entries()) {
                var filesAmount = counter;
                $('#total').text(counter)
            }

            $.each(reserved, function(k, v) {
                if (processed.indexOf(k) <= -1) {
                    Tesseract.recognize('{{ asset('') }}/' + v, {
                        lang: 'eng'
                    })
                    .progress(function(message) {
                        var progress = (100*message.progress).toFixed(2);
                        $('#progress').text(message.status + ' ' + progress + '%')
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .then(function(result) {
                        ctr++;
                        page++;

                        if (page == counter) {
                            $('#addDocuments').removeAttr('disabled');
                        }

                        $('#current').text(page)
                        page_active.push(page);

                        $('input[name="active_pages"]').val(page_active.join(','));

                        var source   = document.getElementById("entry-template").innerHTML;
                        var template = Handlebars.compile(source);
                        var context = {
                            url: v,
                            item: page,
                            img: '{{ asset('') }}/' + v,
                            page: page,
                            conf : result.confidence,
                            text: result.text,
                        };
                        var html    = template(context);
                        $('#page-holder').append(html);

                    })

                    processed.push(k);
                }
            });

       };
    </script>
@endsection
