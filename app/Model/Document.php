<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function contents() {
        return $this->hasMany('App\Model\DocumentContent');
    }

    public function accesses() {
        return $this->hasMany('App\Model\DocumentAccess');
    }
}
