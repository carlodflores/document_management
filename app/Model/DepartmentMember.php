<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DepartmentMember extends Model
{
    public static function get_access($user_id) {
        $dept = DepartmentMember::where('user_id', $user_id)->get();

        $arr = array();
        foreach ($dept as $key => $value) {
            $arr[] = $value->department_id;
        }
        return $arr;
    }

    public function department() {
        return $this->belongsTo('App\Model\Department');
    }

}
