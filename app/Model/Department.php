<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function members() {
        return $this->hasMany('App\Model\DepartmentMember');
    }
}
