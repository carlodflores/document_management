<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            "name"          => 'Dashboard',
            "sub"           => 'Summary of data',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Home',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Dashboard'
                )

            )
        );

        return view('dashboard.index')->with($data);
    }
}
