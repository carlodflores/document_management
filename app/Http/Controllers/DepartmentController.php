<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Department;
use App\Model\DepartmentMember;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {
        $data = array(
            "name"          => 'Departments',
            "sub"           => 'The list of departments',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Departments'
                )

            ),

            "departments"   => Department::all()
        );

        return view('departments.index')->with($data);
    }

    public function showAddForm() {
        $data = array(
            "name"          => 'Add Department',
            "sub"           => 'Add new Department on the System',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Departments',
                    "icon"  => 'fa fa-institution',
                    "url"   => route('departments.all')
                ), array(
                    "name"  => 'Add Department',
                )

            ),

            "departments"   => Department::all()
        );

        return view('departments.add')->with($data);
    }

    public function addProcess(Request $request) {
        $this->validate($request, [
            'department_name'   => 'required'
        ]);

        $dept = new Department;
        $dept->title = $request->department_name;
        $dept->save();

        return redirect(route('departments.all'))->with('success', 'Successfully added new Department.');

    }


    public function showEditForm($department_id) {
        $data = array(
            "name"          => 'Edit Department',
            "sub"           => 'Update the title of the Existing Department.',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Departments',
                    "icon"  => 'fa fa-institution',
                    "url"   => route('departments.all')
                ), array(
                    "name"  => 'Update Department',
                )

            ),

            "department"   => Department::find($department_id)
        );

        return view('departments.edit')->with($data);
    }

    public function editProcess($department_id, Request $request) {
        $this->validate($request, [
            'department_name'   => 'required'
        ]);

        $dept = Department::find($department_id);
        $dept->title = $request->department_name;
        $dept->save();

        return redirect(route('departments.all'))->with('success', 'Successfully Updated  Department.');

    }

    public function deleteProcess($department_id) {
        $dept = Department::find($department_id);
        $dept->delete();

        DepartmentMember::where('department_id', $department_id)->delete();

        return redirect(route('departments.all'))->with('success', 'Successfully Deleted a Department.');
    }
}
