<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Document;
use App\Model\DocumentAccess;
use App\Model\DocumentContent;
use App\Model\Department;
use App\Model\DepartmentMember;
use App\Model\User;


use Illuminate\Support\Facades\App;

class DocumentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index() {
         $data = array(
             "name"          => 'Documents',
             "sub"           => 'The list of all the uploaded documents',
             'breadcrumbs'   => array(
                 array(
                     "name"  => 'Dashbard',
                     "icon"  => 'fa fa-dashboard',
                     "url"   => route('dashboard')
                 ), array(
                     "name"  => 'Documents'
                 )

             ),

             'documents'    => Document::all()
         );

         $data['department'] = function($dept_id) {
             return Department::find($dept_id);
         };

         $data['check_access'] = function() {
             return DepartmentMember::get_access(Auth::user()->id);
         };

         return view('documents.index')->with($data);
     }

     public function view($id) {
         $pdf = App::make('dompdf.wrapper');
         $data['document'] = Document::find($id);
         $view = view('documents.view')->with($data);
         $pdf->loadHTML($view);
         return $pdf->stream();
     }

     public function showAddForm() {
         $data = array(
             "name"          => 'Add New Document',
             "sub"           => '',
             'breadcrumbs'   => array(
                 array(
                     "name"  => 'Dashbard',
                     "icon"  => 'fa fa-dashboard',
                     "url"   => route('dashboard')
                 ), array(
                     "name"  => 'Documents',
                     "url"   => route('documents.all')
                 ), array(
                     "name"  => 'Add New Document'
                 )

             ),

             'departments'  => Department::all(),
             'users'        => User::all()
         );

         return view('documents.add')->with($data);
     }

     public function addProcess(Request $request) {
         $this->validate($request, [
             'data'     => 'required'
         ]);

         $data = $request->data;

        // reform data
        $input = array();

         foreach ($data as $key => $d) {
            if (isset($input[$d['name']])) {
                $input[$d['name']] .= ',' . $d['value'];
            } else {
                $input[$d['name']] = $d['value'];
            }

         }

         $document              = new Document();
         $document->title       = $input['title'];
         $document->description = $input['description'];
         $document->user_id     = Auth::user()->id;
         $document->save();

         //$user_avail    = explode(',', $input['user_avail[]']);
         $departments   = explode(',', $input['departments[]']);

         // foreach ($user_avail as $key => $value) {
         //     $access = new DocumentAccess();
         //     $access->document_id   = $document->id;
         //     $access->type          = 1;
         //     $access->access_id     = $value;
         //     $access->status        = 1;
         //     $access->save();
         // }

         foreach ($departments as $key => $value) {
             $access = new DocumentAccess();
             $access->document_id   = $document->id;
             $access->type          = 1;
             $access->access_id     = $value;
             $access->status        = 1;
             $access->save();
         }

         $active_pages = explode(',', $input['active_pages']);
         foreach ($active_pages as $key => $page) {
             $contents              = new DocumentContent();
             $contents->document_id = $document->id;
             $contents->content     = $input['txt_img_' . $page];
             $contents->image       = $input['url_img_' . $page];
             $contents->page        = $page;
             $contents->save();
         }

         echo json_encode(array('status' => 1, 'url' => route('documents.all')));
     }

     public function editForm($document_id) {
         $data = array(
             "name"          => 'Edit Document',
             "sub"           => '',
             'breadcrumbs'   => array(
                 array(
                     "name"  => 'Dashbard',
                     "icon"  => 'fa fa-dashboard',
                     "url"   => route('dashboard')
                 ), array(
                     "name"  => 'Documents',
                     "url"   => route('documents.all')
                 ), array(
                     "name"  => 'Add New Document'
                 )

             ),

             'departments'  => Department::all(),
             'document'     => Document::find($document_id)
         );

         return view('documents.edit')->with($data);
     }

     public function editProcess($document_id, Request $request) {
         $this->validate($request, [
             'data'     => 'required'
         ]);

         $data = $request->data;

        // reform data
        $input = array();

         foreach ($data as $key => $d) {
            if (isset($input[$d['name']])) {
                $input[$d['name']] .= ',' . $d['value'];
            } else {
                $input[$d['name']] = $d['value'];
            }

         }

         $document              = Document::find($document_id);
         $document->title       = $input['title'];
         $document->description = $input['description'];
         $document->save();

         $active_pages = explode(',', $input['active_pages']);
         $inactive_pages = explode(',', $input['deleted_pages']);

         foreach ($inactive_pages as $key => $content_id) {
             DocumentContent::where('id', $content_id)->delete();
         }

         foreach ($active_pages as $key => $page) {
             if ($input['update_img_' . $page] == 0) {
                 $contents              = new DocumentContent();
                 $contents->document_id = $document->id;
                 $contents->content     = $input['txt_img_' . $page];
                 $contents->image       = $input['url_img_' . $page];
                 $contents->page        = $page;
                 $contents->save();
             } else {
                 $contents              = DocumentContent::find($page);
                 $contents->content     = $input['txt_img_' . $page];
                 $contents->page        = $input['page_img_' . $page];;
                 $contents->save();
             }
         }

         $departments   = explode(',', $input['departments[]']);
         DocumentAccess::where('document_id', $document->id)->delete();
         foreach ($departments as $key => $value) {
             $access = new DocumentAccess();
             $access->document_id   = $document->id;
             $access->type          = 1;
             $access->access_id     = $value;
             $access->status        = 1;
             $access->save();
         }



         echo json_encode(array('status' => 1, 'url' => route('documents.edit.form', $document->id)));


     }

     public function uploadImage(Request $request) {
         $arr = array();
         foreach ($request->file() as $key => $value) {
             $filename  = 'documents/'. $key .'--' . time() . '.jpg';
             $path = public_path()."/". $filename;

             $data = file_get_contents($value);
             if(file_put_contents($path , $data)) {
                 $arr[] = array("key" => $key, "url" => $filename);
             }
         }

         echo json_encode($arr);
     }
}
