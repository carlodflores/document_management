<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Model\User;
use App\Model\Department;
use App\Model\DepartmentMember;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $data = array(
            "name"          => 'Users',
            "sub"           => 'The list of users can access the system.',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Users'
                )

            ),

            "users"   => User::all()
        );

        return view('users.index')->with($data);
    }

    public function showAddForm() {
        $data = array(
            "name"          => 'Add User',
            "sub"           => 'Add new User and authorize them to use the system.',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Users',
                    "icon"  => 'fa fa-users',
                    "url"   => route('users.all')
                ), array(
                    "name"  => 'Add User',
                )
            ),

            "departments"   => Department::all(),
        );

        return view('users.add')->with($data);
    }

    public function addProcess(Request $request) {
        $this->validate($request, [
            "firstname"     => 'required',
            "lastname"      => 'required',
            "email"         => 'required',
            'password'      => 'same:repassword',
            'department'    => 'required|array|min:1',
        ]);

        $user = new User;
        $user->firstname    = $request->firstname;
        $user->middlename   = $request->middlename;
        $user->lastname     = $request->lastname;
        $user->email        = $request->email;
        $user->password     = bcrypt($request->password);
        $user->save();

        foreach ($request->department as $key => $value) {
            $dm = new DepartmentMember();
            $dm->department_id  = $value;
            $dm->user_id        = $user->id;
            $dm->save();
        }

        return redirect(route('users.all'))->with('success', 'Successfully added new User.');

    }

    public function showEditForm($user_id) {
        $data = array(
            "name"          => 'Edit User',
            "sub"           => 'Update User Information.',
            'breadcrumbs'   => array(
                array(
                    "name"  => 'Dashbard',
                    "icon"  => 'fa fa-dashboard',
                    "url"   => route('dashboard')
                ), array(
                    "name"  => 'Users',
                    "icon"  => 'fa fa-users',
                    "url"   => route('users.all')
                ), array(
                    "name"  => 'Edit User',
                )
            ),

            "user"         => User::find($user_id),
            "departments"         => Department::all(),
        );

        $arr = array();
        foreach ($data['user']->departments as $key => $value) {
            $arr[] = $value->department_id;
        }

        $data['dept_id'] = $arr;

        return view('users.edit')->with($data);
    }

    public function editProcess($user_id, Request $request) {
        $this->validate($request, [
            "firstname"     => 'required',
            "lastname"      => 'required',
            "email"         => 'required',
            'department'    => 'required|array|min:1',
        ]);

        $user = User::find($user_id);
        $user->firstname    = $request->firstname;
        $user->middlename   = $request->middlename;
        $user->lastname     = $request->lastname;
        $user->email        = $request->email;

        if ($request->password !== ''
        && $request->password == $request->repassword) {
            $user->password     = bcrypt($request->password);
        }

        $user->save();

        DepartmentMember::where('user_id', $user_id)->delete();

        foreach ($request->department as $key => $value) {
            $dm = new DepartmentMember();
            $dm->department_id  = $value;
            $dm->user_id        = $user->id;
            $dm->save();
        }

        return redirect(route('users.all'))->with('success', 'Successfully Updated a User.');

    }

    public function deleteProcess($user_id) {
        $user = User::find($user_id);
        $user->delete();

        return redirect(route('users.all'))->with('success', 'Successfully Deleted a User.');
    }
}
