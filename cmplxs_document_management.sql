-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2018 at 03:27 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmplxs_document_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'HR Department', 1, NULL, NULL),
(2, 'IT Department', 1, '2018-05-04 01:05:28', '2018-05-04 01:05:28'),
(3, 'Accounting Department', 1, '2018-05-04 01:06:00', '2018-05-04 01:06:00'),
(4, 'Sales Department', 1, '2018-05-04 01:06:06', '2018-05-04 01:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `department_members`
--

CREATE TABLE `department_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department_members`
--

INSERT INTO `department_members` (`id`, `department_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 4, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `title`, `description`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sample Document', 'this is a sample document for something that lorem ipsum', 1, 1, '2018-05-04 20:06:59', '2018-05-04 20:06:59'),
(2, 'Test Document', 'This is a test document', 1, 1, '2018-05-04 20:22:03', '2018-05-04 20:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `document_accesses`
--

CREATE TABLE `document_accesses` (
  `id` int(10) UNSIGNED NOT NULL,
  `document_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_accesses`
--

INSERT INTO `document_accesses` (`id`, `document_id`, `type`, `access_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '2018-05-04 20:06:59', '2018-05-04 20:06:59'),
(2, 1, 1, 2, 1, '2018-05-04 20:06:59', '2018-05-04 20:06:59'),
(3, 2, 1, 3, 1, '2018-05-04 20:22:03', '2018-05-04 20:22:03'),
(4, 2, 1, 4, 1, '2018-05-04 20:22:03', '2018-05-04 20:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `document_contents`
--

CREATE TABLE `document_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `document_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_contents`
--

INSERT INTO `document_contents` (`id`, `document_id`, `content`, `image`, `page`, `created_at`, `updated_at`) VALUES
(1, 1, 'Biography of Lawrence O\'Neal from \"Twigg Family Research\r\n\r\nPertaining to the Life and Times of Robert and Hannah Twigg\"\r\n\r\nby Jerry B. Twigg, copyright 1996.\r\n\r\nFile contributed for use in USGenWeb Archives by Sharon Banzhoff.\r\nshabanz@intrepid.net with permission from Jerry B. Twigg\r\n\r\nUSGENWEB NOTICE: In keeping with our policy of providing free\r\n\r\ninformation on the Internet, data may be freely used by\r\n\r\nnon-commercial entities, as long as this message remains on all\r\n\r\ncopied material. These electronic pages may NOT be reproduced in any\r\n\r\nformat for profit or presentation by other organizations.\r\n__________________________________________________________________\r\n\r\nLawrence O\'Neal:\r\n\r\n\"The story of Lawrence is of particular interest and shows how a turn of fate\r\n\r\nput him in the path of the Twigg family. When Lawrence was a boy his father,\r\nWilliam, sent him along with his younger brother to Thomas Ball, a tailor, for\r\napprenticeship in the trade. Mr. Ball, without family of his own took such a\r\nliking to Lawrence that at a time in 1748 when he became seriously ill, made\r\n\r\nhis will in which he left 197 acres call John of Love to Lawrence. Mr. Ball\r\n\r\ndied before the year was out and by that one act of generosity and affection,\r\ndissuaded Lawrence from a future as a tailor to one of a gentleman land\r\n\r\nowner and marketeer of real estate. When his father died in 1759, seeing that\r\n\r\nhis son was successful in his chosen career, he left his land holdings to\r\nLawrence\'s brothers and to him, a horse. For a short time Lawrence served\r\n\r\nas a clerk of the court for Montgomery County and as sheriff of Frederick\r\n\r\nCounty, but his success would be in the land. By 1793 he owned nearly 3000\r\n\r\nacres in Allegany County alone.\r\n\r\nFive years following the Twigg family move to Sink Hole Bottom, Robert\'s son\r\n\r\nJohn purchased land just to the north and with his wife Rebecca, started their\r\nhome on Three Springs Head. Finding that there was a tract of unsettled land\r\nbetween the two, John contacted Lawrence from whom he purchased Twigg\'s\r\n\r\nAdventure in 1777. The following year Jeremiah Cheney, husband of Naomy\r\n\r\nTwigg, purchased 45 acres on Flintstone Creek called Fat Bacon from Lawrence,\r\nwhich was only six miles or so northeast of Sink Hole Bottom and Discovery.\r\nFollowing the death of Robert Jr., in 1805, his son John acting with his mother as\r\nexecutor of his father\'s estate, found that his father had failed to record the deed\r\nof 1788. In effect the land still belonged to Lawrence. John went to him in 1811\r\nand for the sum of five shillings purchased final rights to the land in his own name\r\nUndoubtedly there were other land purchases that involved Lawrence O\'Neal.\r\n\r\n« Those are yet to be found, but certainly he was an important factor in their lives\r\n(Twiggs). With his wife Henrietta and their seven children, he lived out a com—\r\nfortable life in Montgomery County. The size of his estate can only be measured\r\nby the 28 slaves he owned in 1800. Lawrence died sometime between 1810\r\nand 1820.\" .', 'documents/img_3--1525493157.jpg', 1, '2018-05-04 20:06:59', '2018-05-04 20:06:59'),
(2, 1, 'CONTRACT\r\n\r\n1. All food and beverages shall purchases exclusively from Cravings Food Services, Inc. (hereinaﬁer referred\r\nto as the caterer). 4\r\nIt is agreed and understood that the party named on the reverse side hereof (herein referred to as the\r\nCLIENT) is prohibited to bring in any such food and beverages item.\r\n\r\n2. The Client shall be liable to the CATERER at any loss, damage or injury cause by the act, negligence or the\r\nomission of the CLIENT, his/her representative, contractor, agent, guests/visitors or anyone acting in his/her\r\nbehalf while in the performance of any activity in connection with the contracted function\r\n2.1 Where the CLIENT undertakes or contracts for a special setvup of the function, he must:\r\n\r\na. Ensure that the premises assigned to the function and all properties therewith are protected from\r\ndamages.\r\nb. Ensure that the people working in the project abide by the policy of the CATERER to be issued\r\nseparately.\r\n2.2 Where there is need for rehearsal, the responsibilities stated in 2.] where applicable, shall take effect.\r\n\r\n3. The CLIENT may increase the number of covers by giving the (\'ATERER a written 72 hours (seventy two\r\nhours) prior to the ﬁxed function date. Provided that should the participants exceed by ten percent of the\r\nagreed number, the CATERER shall not be responsible for the insufﬁciency of food.\r\n\r\n4‘ The CLIENT may decrease the number of covers by giving the (‘ATERER a written notice 5 days (five)\r\n\r\n, prior to the fixed function.\r\n\r\n5. The CATERER reserves the absolute right to substitute the ﬁinction space herein contracted by another by\r\ngiving at least 24 hours (twenty four hours) prior notice and such substitute place shall‘be deemed by the\r\nCLIENT as full compliance under this Agreement and for this purpose the C ATERER is no way liable to the\r\nclient.\r\n\r\n6. An authorized representative of the CATERER shall fix and/or establish the number of covers or drinks to\r\nbe served as actually reserved by the CLIENT shown in the reverse side hereof and correspondingly, against\r\nthe bill of the latter.\r\n\r\n7. The CATERER’ also reserves the right to make reasonable substitution on the food/or beverage by giving at\r\nleast 48 hours (forty eight hours) of notice to the CLIENT.\r\n\r\n8. The client shall be billed in accordance with the minimum number of persons ﬁnally contracted notwith-\r\nstanding under attendance or in case of non—appearance of CLIENT. However, should the attendance be\r\nmore than the minimum number stipulated, the CLIENT should be billed per cover or at actual number of\r\npersons present, to be paid right after the function. Any other billing arrangements must be in writing and\r\nduly approved by the General Manager or his duly authorized representative\r\n\r\n9. The CLIENT shall deposit amount equivalent to fifty percent (50%) of the total cost of the contracted\r\nfunction upon signing of the document, and the balance shall be payable at least three working days before\r\nthe function. Additional charges incurred on the day of the function shall be paid on the same day. In the\r\nevent of the breach of cancellation of this agreement by CLIENT, the CATERER shall forfeit the deposit as\r\nfollows:\r\n\r\na, 7 days before the affair = 10% of the deposit shall be refunded.\r\nb. 15 days before the affair = 25% of the deposit shall be refunded.\r\nc. 16 days or more before the affair = 50% of the deposit shall be returned.\r\n\r\n10. The client hereby expressly allows the CATERIER to withhold anything of value belonging to the CLIENT\r\nupon failure of the latter to settle his/its obligation arising from this contract until the CLIENT settles the\r\nobligation.\r\n\r\n11. Should the CATERER institute court action against the CLIENT to enforce payment of unpaid account\r\nunder this Contract or any action arising from this Contract. the CLIENT agrees to pay the attorney‘s fees\r\nequivalent to 25% (twenty-ﬁve percent) of the total unpaid account, but in no case less than 2,000.00\r\nwhether actually incurred or not, plus expense of litigation.\r\n\r\n12. The CATERER shall not be liable for failure to comply with any or all of the terms of this Agreement due\r\nto labor dispute, fortuitous event or other causes beyond its control.\r\n\r\n13. Any item/s (Le. props, backdrop, cakes, etc) brought in to the RESTAURANT in relation to the function, the\r\nRESTAURANT has the right to dispose of the said item/s.\r\n\r\n14. It is hereby understood that catering services for lunch and dinner shall last for a maximum of 5 hours. Any\r\nextension beyond this time shall be subject to a Php 500,00 surcharge per hour.\r\n\r\n15. The CLIENT warrants and represent that he/it has read and fully understood and hereby accept the\r\nforegoing terms and conditions and he/it agrees to comply with the same.\r\n\r\nSigned in the City of , Philippines, this day of ,', 'documents/img_3--1525493157.jpg', 2, '2018-05-04 20:06:59', '2018-05-04 20:06:59'),
(3, 1, '12. check Email uotiﬁoatioussetup, sometimes it doesu\'tseud to the right people and sometimes\r\nthe subject is wvong [for appnwal but release of budget isiu the subject).\r\n13. Noleveryone can vegislevlheve should be maintenance for creating an account in system.\r\n14. Budget FollowdlpEmail Notiﬁoetiou\r\na. llnul process m 3 days autuluiiuwup an budgelluAccuunllng, PmmcenlerDlreaur,\r\nm. Emaii Subleashuuidlnciude ”(Followup)\"\r\nb. llsllii nulprucessed m 7 days autuluiiuwup an budgelluAccuunllng, Pram Center\r\nDlreclur,Admlnand Owners. Emaii Subleashuuidlnciude ”(ESK‘ALATEFDLLDWVUPY‘\r\n15. Halve it mobile leaﬂive if possible.\r\n16. If cunlract needs immediate approval it shuuld aulu send notificaliunlu meil (enlerDirectur,\r\nAccuunling, Owners and Admin. Emali subject shuuid lnciude ”IMMEDIATE BUDGHAPPRDVAL\r\n17. (hangefmm ”ow to ”vuuchel” in paymenltype. Add (heck/(5rd aﬁeld.\r\nmange Ear/nil:\r\nor...“ M ow ,.\r\n|:| <7 Che(k/Cavd/Vau(hem\r\nLs. Add back pageconuacllevms.\r\ncuwmdoa', 'documents/img_3--1525493157.jpg', 3, '2018-05-04 20:07:00', '2018-05-04 20:07:00'),
(4, 2, 'Biography of Lawrence O\'Neal from \"Twigg Family Research\r\n\r\nPertaining to the Life and Times of Robert and Hannah Twigg\"\r\n\r\nby Jerry B. Twigg, copyright 1996.\r\n\r\nFile contributed for use in USGenWeb Archives by Sharon Banzhoff.\r\nshabanz@intrepid.net with permission from Jerry B. Twigg\r\n\r\nUSGENWEB NOTICE: In keeping with our policy of providing free\r\n\r\ninformation on the Internet, data may be freely used by\r\n\r\nnon-commercial entities, as long as this message remains on all\r\n\r\ncopied material. These electronic pages may NOT be reproduced in any\r\n\r\nformat for profit or presentation by other organizations.\r\n__________________________________________________________________\r\n\r\nLawrence O\'Neal:\r\n\r\n\"The story of Lawrence is of particular interest and shows how a turn of fate\r\n\r\nput him in the path of the Twigg family. When Lawrence was a boy his father,\r\nWilliam, sent him along with his younger brother to Thomas Ball, a tailor, for\r\napprenticeship in the trade. Mr. Ball, without family of his own took such a\r\nliking to Lawrence that at a time in 1748 when he became seriously ill, made\r\n\r\nhis will in which he left 197 acres call John of Love to Lawrence. Mr. Ball\r\n\r\ndied before the year was out and by that one act of generosity and affection,\r\ndissuaded Lawrence from a future as a tailor to one of a gentleman land\r\n\r\nowner and marketeer of real estate. When his father died in 1759, seeing that\r\n\r\nhis son was successful in his chosen career, he left his land holdings to\r\nLawrence\'s brothers and to him, a horse. For a short time Lawrence served\r\n\r\nas a clerk of the court for Montgomery County and as sheriff of Frederick\r\n\r\nCounty, but his success would be in the land. By 1793 he owned nearly 3000\r\n\r\nacres in Allegany County alone.\r\n\r\nFive years following the Twigg family move to Sink Hole Bottom, Robert\'s son\r\n\r\nJohn purchased land just to the north and with his wife Rebecca, started their\r\nhome on Three Springs Head. Finding that there was a tract of unsettled land\r\nbetween the two, John contacted Lawrence from whom he purchased Twigg\'s\r\n\r\nAdventure in 1777. The following year Jeremiah Cheney, husband of Naomy\r\n\r\nTwigg, purchased 45 acres on Flintstone Creek called Fat Bacon from Lawrence,\r\nwhich was only six miles or so northeast of Sink Hole Bottom and Discovery.\r\nFollowing the death of Robert Jr., in 1805, his son John acting with his mother as\r\nexecutor of his father\'s estate, found that his father had failed to record the deed\r\nof 1788. In effect the land still belonged to Lawrence. John went to him in 1811\r\nand for the sum of five shillings purchased final rights to the land in his own name\r\nUndoubtedly there were other land purchases that involved Lawrence O\'Neal.\r\n\r\n« Those are yet to be found, but certainly he was an important factor in their lives\r\n(Twiggs). With his wife Henrietta and their seven children, he lived out a com—\r\nfortable life in Montgomery County. The size of his estate can only be measured\r\nby the 28 slaves he owned in 1800. Lawrence died sometime between 1810\r\nand 1820.\" .', 'documents/img_1--1525493945.jpg', 1, '2018-05-04 20:22:03', '2018-05-04 20:22:03'),
(5, 2, 'CONTRACT\r\n\r\n1. All food and beverages shall purchases exclusively from Cravings Food Services, Inc. (hereinaﬁer referred\r\nto as the caterer). 4\r\nIt is agreed and understood that the party named on the reverse side hereof (herein referred to as the\r\nCLIENT) is prohibited to bring in any such food and beverages item.\r\n\r\n2. The Client shall be liable to the CATERER at any loss, damage or injury cause by the act, negligence or the\r\nomission of the CLIENT, his/her representative, contractor, agent, guests/visitors or anyone acting in his/her\r\nbehalf while in the performance of any activity in connection with the contracted function\r\n2.1 Where the CLIENT undertakes or contracts for a special setvup of the function, he must:\r\n\r\na. Ensure that the premises assigned to the function and all properties therewith are protected from\r\ndamages.\r\nb. Ensure that the people working in the project abide by the policy of the CATERER to be issued\r\nseparately.\r\n2.2 Where there is need for rehearsal, the responsibilities stated in 2.] where applicable, shall take effect.\r\n\r\n3. The CLIENT may increase the number of covers by giving the (\'ATERER a written 72 hours (seventy two\r\nhours) prior to the ﬁxed function date. Provided that should the participants exceed by ten percent of the\r\nagreed number, the CATERER shall not be responsible for the insufﬁciency of food.\r\n\r\n4‘ The CLIENT may decrease the number of covers by giving the (‘ATERER a written notice 5 days (five)\r\n\r\n, prior to the fixed function.\r\n\r\n5. The CATERER reserves the absolute right to substitute the ﬁinction space herein contracted by another by\r\ngiving at least 24 hours (twenty four hours) prior notice and such substitute place shall‘be deemed by the\r\nCLIENT as full compliance under this Agreement and for this purpose the C ATERER is no way liable to the\r\nclient.\r\n\r\n6. An authorized representative of the CATERER shall fix and/or establish the number of covers or drinks to\r\nbe served as actually reserved by the CLIENT shown in the reverse side hereof and correspondingly, against\r\nthe bill of the latter.\r\n\r\n7. The CATERER’ also reserves the right to make reasonable substitution on the food/or beverage by giving at\r\nleast 48 hours (forty eight hours) of notice to the CLIENT.\r\n\r\n8. The client shall be billed in accordance with the minimum number of persons ﬁnally contracted notwith-\r\nstanding under attendance or in case of non—appearance of CLIENT. However, should the attendance be\r\nmore than the minimum number stipulated, the CLIENT should be billed per cover or at actual number of\r\npersons present, to be paid right after the function. Any other billing arrangements must be in writing and\r\nduly approved by the General Manager or his duly authorized representative\r\n\r\n9. The CLIENT shall deposit amount equivalent to fifty percent (50%) of the total cost of the contracted\r\nfunction upon signing of the document, and the balance shall be payable at least three working days before\r\nthe function. Additional charges incurred on the day of the function shall be paid on the same day. In the\r\nevent of the breach of cancellation of this agreement by CLIENT, the CATERER shall forfeit the deposit as\r\nfollows:\r\n\r\na, 7 days before the affair = 10% of the deposit shall be refunded.\r\nb. 15 days before the affair = 25% of the deposit shall be refunded.\r\nc. 16 days or more before the affair = 50% of the deposit shall be returned.\r\n\r\n10. The client hereby expressly allows the CATERIER to withhold anything of value belonging to the CLIENT\r\nupon failure of the latter to settle his/its obligation arising from this contract until the CLIENT settles the\r\nobligation.\r\n\r\n11. Should the CATERER institute court action against the CLIENT to enforce payment of unpaid account\r\nunder this Contract or any action arising from this Contract. the CLIENT agrees to pay the attorney‘s fees\r\nequivalent to 25% (twenty-ﬁve percent) of the total unpaid account, but in no case less than 2,000.00\r\nwhether actually incurred or not, plus expense of litigation.\r\n\r\n12. The CATERER shall not be liable for failure to comply with any or all of the terms of this Agreement due\r\nto labor dispute, fortuitous event or other causes beyond its control.\r\n\r\n13. Any item/s (Le. props, backdrop, cakes, etc) brought in to the RESTAURANT in relation to the function, the\r\nRESTAURANT has the right to dispose of the said item/s.\r\n\r\n14. It is hereby understood that catering services for lunch and dinner shall last for a maximum of 5 hours. Any\r\nextension beyond this time shall be subject to a Php 500,00 surcharge per hour.\r\n\r\n15. The CLIENT warrants and represent that he/it has read and fully understood and hereby accept the\r\nforegoing terms and conditions and he/it agrees to comply with the same.\r\n\r\nSigned in the City of , Philippines, this day of ,', 'documents/img_3--1525493157.jpg', 2, '2018-05-04 20:22:03', '2018-05-04 20:22:03'),
(6, 2, '12. check Email uotiﬁoatioussetup, sometimes it doesu\'tseud to the right people and sometimes\r\nthe subject is wvong [for appnwal but release of budget isiu the subject).\r\n13. Noleveryone can vegislevlheve should be maintenance for creating an account in system.\r\n14. Budget FollowdlpEmail Notiﬁoetiou\r\na. llnul process m 3 days autuluiiuwup an budgelluAccuunllng, PmmcenlerDlreaur,\r\nm. Emaii Subleashuuidlnciude ”(Followup)\"\r\nb. llsllii nulprucessed m 7 days autuluiiuwup an budgelluAccuunllng, Pram Center\r\nDlreclur,Admlnand Owners. Emaii Subleashuuidlnciude ”(ESK‘ALATEFDLLDWVUPY‘\r\n15. Halve it mobile leaﬂive if possible.\r\n16. If cunlract needs immediate approval it shuuld aulu send notificaliunlu meil (enlerDirectur,\r\nAccuunling, Owners and Admin. Emali subject shuuid lnciude ”IMMEDIATE BUDGHAPPRDVAL\r\n17. (hangefmm ”ow to ”vuuchel” in paymenltype. Add (heck/(5rd aﬁeld.\r\nmange Ear/nil:\r\nor...“ M ow ,.\r\n|:| <7 Che(k/Cavd/Vau(hem\r\nLs. Add back pageconuacllevms.\r\ncuwmdoa', 'documents/img_1--1525449367.png', 3, '2018-05-04 20:22:03', '2018-05-04 20:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_06_002724_create_document_table', 1),
(4, '2018_04_06_002746_create_department_table', 1),
(5, '2018_04_06_030638_create_document_content_table', 1),
(6, '2018_05_04_072432_create_document_access_table', 1),
(7, '2018_05_04_072547_create_department_member_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `image`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John', NULL, 'Doe', 'admin@complexus.net', '$2y$10$5gh7GblzqjSrQmT1EM3JieXASC8xnhhbWOX6XWaCBbeHuZeuuKEZu', NULL, 1, NULL, NULL, NULL),
(2, 'Jane', NULL, 'Doe', 'jane@complexus.net', '$2y$10$5gh7GblzqjSrQmT1EM3JieXASC8xnhhbWOX6XWaCBbeHuZeuuKEZu', NULL, 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_members`
--
ALTER TABLE `department_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_accesses`
--
ALTER TABLE `document_accesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_contents`
--
ALTER TABLE `document_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `department_members`
--
ALTER TABLE `department_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `document_accesses`
--
ALTER TABLE `document_accesses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `document_contents`
--
ALTER TABLE `document_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
